import * as crypto from 'crypto';
import Tools from './Tools.js';
import * as jstools from 'jstools';

export default class Transaction {

	constructor(amount, payer, payee, msg = '') {
		this.payer = payer;
		this.amount = amount;
		this.payee = payee;
		this.msg = msg;
		this.id = Tools.newTransactionId;
	}

	toString() {
		return JSON.stringify(this);
	}

	get hash() {
		return jstools.ohash(this);
	}

}

