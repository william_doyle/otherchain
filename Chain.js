import * as crypto from 'crypto';
import Tools from './Tools.js';
import Transaction from './Transaction.js';
import Block from './Block.js'
import Settings from './Settings.js';
import TransactionBatch from './TransactionBatch.js';
import {promises as fs} from 'fs';
import IterateTransactions from './ChainFunctions/IterateTransactions.js';
import AddBatch from './ChainFunctions/AddBatch.js';
import * as jstools from 'jstools';


export default class Chain {
	static instance = new Chain(); 

	chain; // array of blocks
	static #chain_origin = undefined;
	static #testMode = false;

	constructor() {
		this.chain = [new Block(null, [new Transaction(1, 'alice', 'bob', '---GENESIS BLOCK---')])];
	}

	static get lastBlock() {
		return Chain.instance.chain[Chain.instance.chain.length - 1];
	}

	static setTestMode() {
		Chain.#testMode = true;
		Chain.instance = new Chain();
	}

 	static async addBatch(block, minorPublicKey, signature, solution, senderSigs) {
 		await AddBatch(block, minorPublicKey, signature, solution, senderSigs, {
			self: {
				blockArray: Chain.instance.chain,
				chainFile: Chain.chain_origin ?? 'chain.json',
				fs: fs,
			},
			saveChain: async function () {
				await Chain.save(); 
			},
			pushBlock: function (block) {
				Chain.instance.chain.push(block);
			},
			stringifySelf: function () {
				return JSON.stringify(Chain.#chain);
			}
		}); 
	}

 	/*	getAddressValue()
	 *	get sum of transactions in and out of the provided address
	 *	William Doyle
	 * */
	static getAddressValue(address) {
		let count = 0;
		function countValue(trs) {
				if (trs.payer === address)
					count -= trs.amount;
				else if (trs.payee === address)
					count += trs.amount;
		}
		IterateTransactions(Chain.instance.chain, countValue);
		return count;
	}

	/*	getTotalMined()
	 *	count the amount that has been mined
	 *	William Doyle
	 * */
	static getTotalMined() {
		let count = 0;
		function tally (trs) {
			if (trs.payer === null)
				count += trs.amount;
		}
		IterateTransactions(Chain.instance.chain, tally);
		return count;
	}

	/*	hasRepeatedTransactions()
	 *	Check for transactions with the same id and or same hash
	 *	William Doyle
	 *	pre Aug 14th 2021
	 * */
	static hasRepeatedTransactions() {
		let found_duplicate_transaction = false;
		const tids = [];
		Chain.#chain.forEach(b => {
			b.transactions.forEach(t => {
				if (tids.includes(t.id)) {
					console.error(`DOUBLE SPEND DETECTED: transaction id -> ${t.id}`);
					found_duplicate_transaction = true;
				}
				else
					tids.push(t.id);
			})
		});

		return found_duplicate_transaction;
	}

	/*	save()
	 *	save the chain to the file system
	 *	William Doyle
	 * */
	static async save(fname = 'chain.json') {
		if (Chain.#testMode)
			return;
		let _fname = fname;
		if (Chain.#chain_origin !== undefined) 
			_fname = Chain.#chain_origin;

		const state = JSON.stringify(Chain.#chain);
		const status = await fs.writeFile(_fname, state);
	}

	/*	loadFromDisk()
	 *	load the chain from a file
	 *	William Doyle
	 * */
	static async loadFromDisk(fname = 'chain.json') {
		if (Chain.#testMode)
			return;
		Chain.#chain_origin = fname;
		const chainjson = await fs.readFile(fname, 'utf8');
		const arr = JSON.parse(chainjson);
		Chain.instance.chain = arr.map(b => Block.fromOBJ(b));
	}

	static get length () {
		return Chain.#chain.length;
	}

	static get numTransactions () {
		return Chain.instance.chain.reduce((acc, blok) => acc + blok.transactions.length, 0 );
	}

	static get chain () {
		return Chain.instance.chain;
	}

	// hidden short cut to get chain
	static get #chain () {
		return Chain.instance.chain;
	}
}

