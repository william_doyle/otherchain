export default class Settings {

	static get blockReward() {
		return 0.1;
	}

	static get difficulty() {
		return 4; // 5 leds to alloc failure... lol
	}

	static zerosString () {
		let s = '';
		for (let i = 0; i < Settings.difficulty; i++)
			s = `${s}0`;
		return s;
	}
}
