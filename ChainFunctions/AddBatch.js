import Settings from '../Settings.js';
import * as crypto from 'crypto';
import Tools from '../Tools.js';
import Transaction from '../Transaction.js';
import * as jstools from 'jstools';


export default async function AddBatch(block, minorPublicKey, signature, solution, senderSigs, callbacks) {

	// 1. check transactions are valid

	// 1.0 num of transactions is same as num of sigs
	if(!batchContainsExactlyAllowedReward(block.transactions))
		return rejectBlock(`Bad tranasctions. or miner tried to steal.`);

	{
		const numSigs = senderSigs.length;
		const numTransactions = block.transactions.length;
		if ((numSigs + 1 !== numTransactions) && (numSigs !== numTransactions))
			return	rejectBlock(`Signature count does not match transaction count (${numSigs}, ${numTransactions})`);
	}

	// 1.1 verify every transactions signature
	
	// check that only 1 transaction comes from null address and it is exactly the right amount of reward
	{
		const cleanedTrans = block.transactions.filter(t => t.payer !== null);
		for (let i = 0; i < cleanedTrans.length; i++) 
			if (!jstools.checkSignature(cleanedTrans[i].toString(), cleanedTrans[i].payer, senderSigs[i])) 
				return rejectBlock(`bad signature found in block`);
	}

	// 1.2
	// reject if block already mined or if block prev hash does not match
	for (let i = 0; i < callbacks.self.blockArray.length; i++) 
		if (callbacks.self.blockArray[i].hash === block.hash) 
			return rejectBlock(`block already mined`);

	// check these exact transactions have not happened before
	// GET ALL PREVIOUS TRANSACTION HASHs
	const transHashes = callbacks.self.blockArray.flatMap(b => b.transactions.map(t => Object.assign(new Transaction(), t).hash));

	if (transHashes.length !== [...new Set(transHashes)].length) 
		console.error(`DUPLICATE TRANSACTION WAS IN CHAIN WHEN WE FOUND IT`, transHashes);

	block.transactions.forEach(t => transHashes.push(t.hash));
	if (transHashes.length !== [...new Set(transHashes)].length) 
		return rejectBlock(`Transaction duplicates`);

	// 2. check proof
	// We hash minors solution + blocks nonce and check that the resulting hash starts with correct number of zeros
	{
		const attempt = Tools.hash(`${block.nonce + solution}`);
		if (attempt.substr(0, Settings.difficulty) !== Settings.zerosString()) 
			return rejectBlock(`bad proof`);
	}

	// 3. accept block
	acceptBlock();
	callbacks.pushBlock(block);
	callbacks.saveChain();
	console.log(`💾 saved`);
	return 1;
}

function rejectBlock(reason) {
	console.log(`🖕 Rejecting block! reason: ${reason}`);
	return -1;
}

function acceptBlock() {
	console.log(`✅ Accepting block`);
	return 1;
}

function batchContainsExactlyAllowedReward(transactions) {

	// 1. only 1 transactions may be from null
	const count = transactions.reduce((acc, t) => acc + (t.payer === null ? 1 : 0), 0);
	if (count > 1) {
		console.warn(`More than 1 transaction in this batch was sent from null. Rejected`);
		return false;
	}

	// 2. find the 1 reward transaction and check that it is not more than the legal reward
	const rewardTranaction = transactions.find(t => t.payer === null);
	if (rewardTranaction.amount > Settings.blockReward){
		console.warn(`Minor tried to set reward to non standard value. This was an attempt at theft!`);
		return false;
	}

	return true; // miner is a good boy
}

