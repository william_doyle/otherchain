export default function IterateTransactions(chain, callback) {
	for (let i = 0; i < chain.length; i++)
		for (let k = 0; k < chain[i].transactions.length; k++)
			callback(chain[i].transactions[k]);
}
