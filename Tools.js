import * as crypto from 'crypto';
import * as jstools from 'jstools';

export default class Tools {

	static get nextSalt() {
		return Math.random();
	}
	
	static get newTransactionId() {
		return Math.random();
	}

	/*	Quick and dirty hash function
	 *
	 * */
	static hash(input) {
		return jstools.hash(input);
	}
}
