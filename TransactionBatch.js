import Transaction from './Transaction.js';

export default class TransactionBatch {

	constructor() {
		this.transactions = [];
		this.sigs = [];
	}

	addTransaction(transaction, sig){
		this.transactions.push(transaction);
		this.sigs.push(sig);
	}
	
	toString() {
		return JSON.stringify(this);
	}
}
