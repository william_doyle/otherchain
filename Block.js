import * as crypto from 'crypto';
import * as jstools from 'jstools';

export default class Block {
	nonce = Math.round(Math.random() * 99999999999);

	constructor(prevHash, transactions, timestamp) {
		this.transactions = transactions;
		this.prevHash = prevHash;
		this.timestamp = timestamp;
	}

	get hash() {
		return jstools.ohash(this);
	}

	static fromJSON(json) {
		return Object.assign(new Block(null, null, null), JSON.parse(json));
	}

	static fromOBJ(obj) {
		return Object.assign(new Block(null, null, null), obj);
	}
}

