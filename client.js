import Wallet from './Wallet.js';
import Chain from './Chain.js';

export default class Client {

	static #wallet;

	/*	createNew
	 *	create a new client and save the private and public keys to a file
	 * */
	static async createNew(){
		const { publickey, privatekey } = new Wallet();
		const wallet_f_name = `wallets/wallet_${new Date().getTime()}_.json`;
		console.log(`Saving your wallet to '${wallet_f_name}'`);
		await w.save(wallet_f_name);
		return w;
	}

	static async load(fname) {
		const swallet = await Wallet.loadJson(fname);
		Client.#wallet = Wallet.from(swallet);
		return Client.#wallet;
	}

	static async LoadDefaults() {
		await Promise.all([
			Client.load('default_wallet.json'),
			Chain.loadFromDisk(),
		]);
		return Client.#wallet;
	}
}
