import TransactionBatch from './TransactionBatch.js';
import Wallet from './Wallet.js';
import Settings from './Settings.js';
import Block from './Block.js'
import Chain from './Chain.js';
import * as crypto from 'crypto';
import Transaction from './Transaction.js';

export default class Minor extends Wallet {

	constructor(evilmode_extrareward = false, evilmode_multiplereward = false) {
		super();
		this.evilmode_extrareward = evilmode_extrareward; 
		this.evilmode_multiplereward = evilmode_multiplereward;
	}

	async processTransactionBatch(transactionBatch) {
		const ts = [
			...transactionBatch.transactions,
			// minor is allowd to add single payment from null to self
			new Transaction(Settings.blockReward * (this.evilmode_extrareward? 2:1), null, this.publickey, 'STANDARD REWARD'),
		];

		if (this.evilmode_multiplereward){
			ts.push(new Transaction(Settings.blockReward, null, this.publickey));
		}

		//const block = new Block(Chain.instance.lastBlock.hash, ts		); 
		const block = new Block(Chain.lastBlock.hash, ts		); 

		const solution = this.mine(block.nonce, this.publickey); 
		console.log(`✍️  Minor signing block...`);

		const sign = crypto.createSign('SHA256');
		sign.update(transactionBatch.toString()).end();
		const signature = sign.sign(this.privatekey);

		//Chain.instance.addBatch(block, this.publickey, signature, solution, transactionBatch.sigs);
		console.info(`🔗 Asking chain to add block`);
		const success_code = await Chain.addBatch(block, this.publickey, signature, solution, transactionBatch.sigs);

		console.info(`Chain gave success code of ${success_code}`);
		return success_code;
	}

	// mine on own dime... minors have to run this function locally
	mine(nonce, miner) {
		console.log(`⛏️  mining | MINER`);

		for(let solution = 1;; solution++) {

			const hash = crypto.createHash('SHA256');
			hash.update((nonce + solution).toString()).end();

			const attempt = hash.digest('hex');

			if (attempt.substr(0, Settings.difficulty) === Settings.zerosString()) {
				console.log(`🎉 Solved: ${solution} attempt: ${attempt}`);
				return solution;
			}
		}
	}


}


