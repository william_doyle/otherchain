import Wallet from '../Wallet.js';
import Chain from '../Chain.js';
import Settings from '../Settings.js';
import Miner from '../Miner';
import TransactionBatch from '../TransactionBatch.js';
import Block from '../Block.js';

describe('basic functionality', () => {

	it('', () => {
		const theJSON = `{"nonce":62625282404,"transactions":[{"payer":"alice","amount":1,"payee":"bob","msg":"---GENESIS BLOCK---","id":0.002164651138170326}],"prevHash":null}`;
		const _b = Block.fromJSON(theJSON);
		expect(_b instanceof Block).toBe(true);

		expect(_b.hash).toBe('47a6f4aed094d956fdfc2788266d85ffbfc1043b8e653bfae79e12ef5d0876eb'); // precalculated -- might need updating of you change bloclk class
	});

});


describe('batch transactions and miner mining', () => {
	
	it('simple batch',async  () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m = new Miner();

		const a = new Wallet();
		const b = new Wallet();
		const c = new Wallet();

		const tbatch = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig); 

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(c.value).toEqual(0);
		expect(m.value).toEqual(0);

		const success_code = await m.processTransactionBatch(tbatch);

		console.log(`success_code is ${success_code}`);

		console.log(`a value is ${a.value}`);
		console.log(`b value is ${b.value}`);
		console.log(`c value is ${c.value}`);

		expect(a.value).toEqual(-1);					// because a sent 1 to b
		expect(b.value).toEqual(1);						// because b got 1 from a
		expect(c.value).toEqual(0);						// because c was involved in no transactions
		expect(m.value).toEqual(Settings.blockReward);	// because m mined 1 block

		expect(Chain.getTotalMined()).toEqual(m.value);
		console.log(`m|minor value is ${m.value}`);
	});

	it('Minor in evilmode cannot steal with big reward', async () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m = new Miner(true);						// miner with evilmode set to true

		const a = new Wallet();
		const b = new Wallet();

		const tbatch = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig);

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(m.value).toEqual(0);

		await m.processTransactionBatch(tbatch);

		console.log(`a value is ${a.value}`);
		console.log(`b value is ${b.value}`);
		console.log(`m value is ${m.value}`);

		expect(m.value).toEqual(0);					// because m mined 1 block but transaction should fail so 0
		expect(a.value).toEqual(0);					// because a sent 1 to b but transaction should fail so 0
		expect(b.value).toEqual(0);					// because b got 1 from a but transaction should fail so 0

		console.log(`m|minor value is ${m.value}`);
	});


	it('Minor in evilmode cannot steal with multiple reward', async () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m = new Miner(false, true);						// miner with evilmode set to true

		const a = new Wallet();
		const b = new Wallet();

		const tbatch = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig);

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(m.value).toEqual(0);

		await m.processTransactionBatch(tbatch);

		console.log(`a value is ${a.value}`);
		console.log(`b value is ${b.value}`);
		console.log(`m value is ${m.value}`);

		expect(m.value).toEqual(0);					// because m mined 1 block but transaction should fail so 0
		expect(a.value).toEqual(0);					// because a sent 1 to b but transaction should fail so 0
		expect(b.value).toEqual(0);					// because b got 1 from a but transaction should fail so 0

		console.log(`m|minor value is ${m.value}`);
	});

	it('Second miner also tried to mine block',async  () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m1 = new Miner();
		const m2 = new Miner();

		const a = new Wallet();
		const b = new Wallet();
		const c = new Wallet();

		const tbatch = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig);

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(c.value).toEqual(0);
		expect(m1.value).toEqual(0);
		expect(m2.value).toEqual(0);

		await m1.processTransactionBatch(tbatch);
		await m2.processTransactionBatch(tbatch);

		console.log(`a value is ${a.value}`);
		console.log(`b value is ${b.value}`);
		console.log(`c value is ${c.value}`);

		expect(a.value).toEqual(-1);					// because a sent 1 to b
		expect(b.value).toEqual(1);						// because b got 1 from a
		expect(c.value).toEqual(0);						// because c was involved in no transactions
		expect(m1.value).toEqual(Settings.blockReward);	// because m mined 1 block
//		expect(m2.value).toEqual(Settings.blockReward);	// because m mined 1 block

		console.log(`m|minor value is ${m1.value}`);
	});

	it('duplicate transaction in same block', async () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m1 = new Miner();

		const a = new Wallet();
		const b = new Wallet();

		const tbatch = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig);
		tbatch.addTransaction(trans, sig);

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(m1.value).toEqual(0);

		await m1.processTransactionBatch(tbatch);

		expect(a.value).toEqual(0);			// because transaction happend twice in same block block is rejected
		expect(b.value).toEqual(0);			// see above
		expect(m1.value).toEqual(0);		// see above

		console.log(`m|minor value is ${m1.value}`);
	});

	it('duplicate transaction in different block', async () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m1 = new Miner();

		const a = new Wallet();
		const b = new Wallet();

		const tbatch = new TransactionBatch();
		const tbatch2 = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig);

		tbatch2.addTransaction(trans, sig);

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(m1.value).toEqual(0);

		await m1.processTransactionBatch(tbatch);
		m1.processTransactionBatch(tbatch2);

		// first transaction passes... second fails
		expect(a.value).toEqual(-1);				
		expect(b.value).toEqual(1);						
		expect(m1.value).toEqual(Settings.blockReward);

		console.log(`m|minor value is ${m1.value}`);
	});

	it('transactions do not appear twice in the chain', async () => {
		Chain.setTestMode();
		expect(Chain.hasRepeatedTransactions()).toEqual(false);
	});

	it('chain can be saved', async () => {

		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m = new Miner();

		const a = new Wallet();
		const b = new Wallet();
		const c = new Wallet();

		const tbatch = new TransactionBatch();

		const {trans, sig} = a.createTransaction(1, b.publickey)
		tbatch.addTransaction(trans, sig); 

		// expect all values to be 0 because miner has not yet mined
		expect(a.value).toEqual(0);
		expect(b.value).toEqual(0);
		expect(c.value).toEqual(0);
		expect(m.value).toEqual(0);

		await m.processTransactionBatch(tbatch);

		console.log(`a value is ${a.value}`);
		console.log(`b value is ${b.value}`);
		console.log(`c value is ${c.value}`);

		expect(a.value).toEqual(-1);					// because a sent 1 to b
		expect(b.value).toEqual(1);						// because b got 1 from a
		expect(c.value).toEqual(0);						// because c was involved in no transactions
		expect(m.value).toEqual(Settings.blockReward);	// because m mined 1 block

		await Chain.save();
	});

	it.skip('chain can restored from disk', async () => {
		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
	});

	it('cannot access private members of class ', () => {
		Chain.setTestMode();
		expect('#chain' in Chain).toEqual(false);
		expect('chain' in Chain).toEqual(true);
	});

	it('get transaction history and larger chain', async () => {
	

		Chain.setTestMode();
		await Chain.loadFromDisk('chain__test.json');
		const m1 = new Miner();	
		const m2 = new Miner();	
		const m3 = new Miner();	
		const m4 = new Miner();	
		const m5 = new Miner();	
		const m6 = new Miner();	
		const a = new Wallet();
		const b = new Wallet();
		const c = new Wallet();
		const d = new Wallet();
		const e = new Wallet();
		const f = new Wallet();
		const g = new Wallet();
		const h = new Wallet();

		const wallets = [
			m1,	
			m2,	
			m3,	
			m4,	
			m5,	
			m6,	
			a,
			b,
			c,
			d,
			e,
			f,
			g,
			h,
		];

		{
			const tbatch = new TransactionBatch();
			{
				const {trans, sig} = a.createTransaction(1, f.publickey)
				tbatch.addTransaction(trans, sig);
			}
			{
				const {trans, sig} = a.createTransaction(1.1, b.publickey)
				tbatch.addTransaction(trans, sig);
			}
			{
				const {trans, sig} = a.createTransaction(1.2, a.publickey)
				tbatch.addTransaction(trans, sig);
			}
			{
				const {trans, sig} = a.createTransaction(1.3, b.publickey)
				tbatch.addTransaction(trans, sig);
			}
			await m1.processTransactionBatch(tbatch);
		}
	{
			const tbatch = new TransactionBatch();
			{
				const {trans, sig} = b.createTransaction(0.1, a.publickey)
				tbatch.addTransaction(trans, sig);
			}
			{
				const {trans, sig} = c.createTransaction(0.31, m3.publickey)
				tbatch.addTransaction(trans, sig);
			}
			{
				const {trans, sig} = a.createTransaction(0.2, b.publickey)
				tbatch.addTransaction(trans, sig);
			}
			{
				const {trans, sig} = b.createTransaction(0.3, h.publickey)
				tbatch.addTransaction(trans, sig);
			}
			await m2.processTransactionBatch(tbatch);
			await m1.processTransactionBatch(tbatch);
		}


		expect(Chain.hasRepeatedTransactions()).toEqual(false);
		const historya = a.transactionHistory;
		console.log(`history is ------------------`);
		console.log(historya);


	});


})

/*
describe('simple blockchain', () => {


	it.('expected reward is minded', () => {

		const alice = new Wallet();
		const bob = new Wallet();
		const num_trans = 10;

		expect(Chain.instance.getTotalMined()).toEqual(0)
	

		for (let i = 0; i <= num_trans; i++)
			alice.sendMoney(1, bob.publickey);
		
		// fails because compairing floats like ths is bad
		expect(Chain.instance.getTotalMined()).toEqual(num_trans * Settings.blockReward)


	})

	it.('transaction changes wallet balance', () => {

		const alice = new Wallet();
		const bob = new Wallet();
		const chuck = new Wallet();

		alice.sendMoney(1, bob.publickey);
		alice.sendMoney(1, bob.publickey);
		alice.sendMoney(1, bob.publickey);
		alice.sendMoney(1, bob.publickey);


		const lblock = Chain.instance.chain[Chain.instance.chain.length - 1];

		expect(lblock.prevHash).toEqual(Chain.instance.chain[Chain.instance.chain.length - 2].hash)
		expect(bob.value).toEqual(Chain.instance.getAddressValue(bob.publickey))

		console.log(lblock);
		console.log(`Alice has ${alice.value} coin`);
		console.log(`Bob has ${bob.value} coin`);
		console.log(`${1 + (bob.value + alice.value)} vs ${1 * 0.1}`);
		

		console.log(`All mined: ${Chain.instance.getTotalMined()}`);
	})


})

*/



describe('test the client', () => {

	it('client can send transactions', () => {


	});

	it('client can show balance', () => {


	});




});
