import {promises as fs} from 'fs';

export default class Saveable {

	constructor() {}

	async save(fname) {
		const state = JSON.stringify(this);
		const status = await fs.writeFile(fname, state);
	}

	static async loadJson(fname) {
		return await fs.readFile(fname);
	}
}
