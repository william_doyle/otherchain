import * as crypto from 'crypto';
import Transaction from './Transaction.js';
import Block from './Block.js';
import Chain from './Chain.js';
import Saveable from './Saveable.js';
import * as jstools from 'jstools';

export default class Wallet extends Saveable {

	publickey;
	privatekey;

	constructor() {
		super();
		const keypair = jstools.GenerateKeys(); 
		this.publickey = keypair.publicKey;
		this.privatekey = keypair.privateKey;
	}

	/*	createTransaction()
	 *	Create a new transaction
	 *	William Doyle
	 * */
	createTransaction(amount, payeePublicKey) {
		const trans = new Transaction(amount, this.publickey, payeePublicKey);
		const signature = jstools.sign(trans.toString(), this.privatekey);
		return {trans: trans, sig:signature };
	}

	get value() {
		if (! this.publickey) 
			console.error(`Wallet::value() [getter] this.publickey is missing!`);

		return Chain.getAddressValue(this.publickey);
	}

	get transactionHistory() {
		const transactionHistory = [];
		Chain.chain.forEach((b, i) => {
			b.transactions.forEach((t) => {
				if ((t.payer === this.publickey ) || (t.payee === this.publickey))
					transactionHistory.push({transaction: t, block: {index: i, hash: b.hash}});
			});
		});
		return transactionHistory;
	}

	static from(json) {
		return Object.assign(new Wallet(), JSON.parse(json));
	}
}
