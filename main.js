import Client from './client.js';
import * as readlineSync from 'readline-sync';
import Chain from './Chain.js';
import Miner from './Miner.js';
import TransactionBatch from './TransactionBatch.js';

function c (obj) {
	return JSON.parse(JSON.stringify(obj));
}

async function send(state) {

	console.log(`------------------------SEND FUNDS------------------------`);
	const to = readlineSync.question('TO: ');
	const amount = readlineSync.question('Amount: ');
	console.log(`----------------------------------------------------------`);

	// create transfer and notify network so it can be processed by a miner

	const {trans, sig} = state.wallet.createTransaction(Number(amount), to);
	const m = new Miner();
	const tbatch = new TransactionBatch();
	tbatch.addTransaction(trans, sig); 
	await m.processTransactionBatch(tbatch);

	// report when confirmed
	function confirmationCallback(confirmations) {
		// TODO: use padding instead of tabs
		console.log(`\n\n------------------------SEND FUNDS------------------------`);
		console.log(`TO:\t\t${to}`);
		console.log(`AMOUNT:\t\t${amount}`);
		console.log(`VERIFIED:\t${confirmations}`);
		console.log(`----------------------------------------------------------`);
	}

	//confirmationCallback(50);

}

async function main () {

	const state = {
		quit: false,
		command: 'startup',
		wallet: undefined,
	};

	const OPTIONS = Object.freeze({
		STARTUP: 'startup',
		QUIT: 'quit',
		HELP: 'help',
		NEW: 'new',
		RECEIVE: 'receive',
		SEND: 'send',
		BALANCE: 'balance',
		SET_DEFAULT: 'set default',
		LOAD_DEFAULT: 'load default',
		STATS: 'stats',
	});

	// load default chain and wallet
	state.wallet = await Client.LoadDefaults(); 	// load default chain and wallet and return wallet

//	state.wallet = await Client.load('wallets/wallet_1629573648151_.json');

	for (; !state.quit ; state.command = readlineSync.question('>> ')) {
		switch (state.command) {
			case OPTIONS.STARTUP:
				console.log('startup message here');
				break;
			case OPTIONS.QUIT:
				state.quit = true;
				return;
				break;
			case OPTIONS.HELP:
				console.log('OPTIONS --> ', Object.values(OPTIONS));
				break;
			case OPTIONS.NEW:
				// create a new wallet
				state.wallet = await Client.createNew();
				console.log('you now have a wallet');
				break;

			case OPTIONS.RECEIVE:
				if (! state.wallet){
					console.warn(`no wallet found, create a 'new' wallet or load a new one from disk`);
					break;
				}
				console.log(`Your wallet address is...`);
				console.log(state.wallet.publickey);
				break;
			case OPTIONS.SEND:
				await send(state);
				break;
			case OPTIONS.BALANCE:
				if (!state.wallet){
					console.log('You need a wallet to have a blance');
					break;
				}
				console.log(`balance: ${state.wallet.value}`);
				break;
			case OPTIONS.SET_DEFAULT:
				if (!state.wallet){
					console.log('You need a wallet to set your wallet as default');
					break;
				}
				await state.wallet.save(`default_wallet.json`);
				console.log(`set wallet ${state.wallet.publickey} as default`);
				break;
			case OPTIONS.LOAD_DEFAULT:
				state.wallet = await Client.load('default_wallet.json');
				console.log(`done`);
				break;
			case OPTIONS.STATS:
				console.log(`TOTAL MINED:\t${Chain.getTotalMined()}`);
				console.log(`NUM BLOCKS:\t${Chain.length}`);
				console.log(`NUM TRANS:\t${Chain.numTransactions}`);
				break;
			default: 
				console.log(`unknown command`);
		}
	}
}

main();
